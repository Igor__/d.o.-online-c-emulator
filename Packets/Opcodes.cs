﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gameServer.Packets
{
	static class RecvOps
	{
        public static readonly byte Validate            = 0x45;
		public static readonly byte Ping                = 0x0C;
		public static readonly byte CreateNewCharacter  = 0x2E;
		public static readonly byte OnPlayaQuit         = 0x08;
        public static readonly byte OnPlayaReqSpawn     = 0x09;
        public static readonly byte OnPlayaMove         = 0x18;
	}

	static class SendOps
    {
        public static readonly byte[] LobbyOneChar = new byte[5] { 0x98, 0x02, 0x0, 0x0, 0x03 };
        public static readonly byte[] LobbyTwoChar = new byte[5] { 0x25, 0x05, 0x0, 0x0, 0x03 };
        public static readonly byte[] LobbyThrChar = new byte[5] { 0xB2, 0x07, 0x0, 0x0, 0x03 };
        public static readonly byte[] LobbyFouChar = new byte[5] { 0x3F, 0x0A, 0x0, 0x0, 0x03 };
        public static readonly byte[] LobbyFivChar = new byte[5] { 0xCC, 0x0C, 0x0, 0x0, 0x03 };
		//public static readonly short CheckPassword = 0x0000;
	}
}
