﻿using gameServer.Core.IO;
using gameServer.Game.Map.Objects;
using gameServer.Game.Objects;
using gameServer.Tools;
using gameServer.Packets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gameServer.Game.Map
{
    class Map
    {
        public int ID { get; set; }
        public int ForcedReturn { get; set; }
        public int ReturnMap { get; set; }
        public bool Town { get; set; }
        public bool HasClock { get; set; }
        public byte FieldType { get; set; }
        public bool Fly { get; set; }
        public bool Swim { get; set; }
        public int MobRate { get; set; }
        public string OnFirstUserEnter { get; set; }
        public string OnUserEnter { get; set; }

        public int WeatherID { get; set; }
        public string WeatherMessage { get; set; }
        public bool WeatherAdmin { get; set; }

        const uint NpcStart = 100;
        const uint ReactorStart = 200;

        public LoopingID ObjectIDs { get; set; }

        public List<Character> Characters { get; set; }

        public Map(int id)
        {
            ID = id;
            ObjectIDs = new LoopingID();
            //_Mobs = new list<Mob>();
            Characters = new List<Character>();
        }

        public Character GetPlayer(int id)
        {
            try
            {
                Character ret = null;
                Characters.ForEach(c => { if (c.Id == id) ret = c; });
                return ret;
            }
            catch { }
            return null;
        }

        public void RemovePlayer(Character chr)
        {
            if (Characters.Contains(chr))
            {
                Characters.Remove(chr);

                UpdateMobControl(chr);
                //PetsPacket.SendRemovePet(chr);
                //Characters.ForEach(p => { MapPacket.SendCharacterLeavePacket(chr.Id, p); });
            }
        }

        public void AddPlayer(Character chr)
        {
            Characters.Add(chr);
            ShowObjects(chr);
        }

        public void SendPacket(InPacket packet)
        {
            SendPacket(packet, null, false);
        }

        public void SendPacket(InPacket packet, Character skipme, bool log)
        {
            Characters.ForEach(p =>
            {
                if (p != skipme)
                    p.Client.WritePacket(packet.ToArray());
                else if (log)
                    Console.WriteLine("Not sending packet to charid {0} (skipme: {1})", p.Id, skipme.Id);
            });
        }

        public void ShowObjects(Character chr)
        {
            //if (chr.Pets.GetEquippedPet() != null)
            //{
            //    Pet pet = chr.Pets.GetEquippedPet();
            //    pet.mPosition = new Pos(chr.Position);
            //    pet.mFoothold = chr.Foothold;
            //    pet.mStance = 0;
            //}
            //chr.Pets.SpawnPet();
            Characters.ForEach(p =>
            {
                if (p != chr)
                {
                    if (Functions.GetDistanceBetweenPoints(chr.locX, chr.locY, p.locX, p.locY) < 300)
                    {

                    }

                    //MapPacket.SendCharacterEnterPacket(p, chr);
                    //MapPacket.SendCharacterEnterPacket(chr, p);
                    //p.mPets.SpawnPet(chr);
                    //if (p.mSummons.mSummon != null) SummonPacket.SendShowSummon(p, p.mSummons.mSummon, false, chr);
                    //if (p.mSummons.mPuppet != null) SummonPacket.SendShowSummon(p, p.mSummons.mPuppet, false, chr);
                }
            });
            //Life["n"].ForEach(n => { FieldPacketCreator.ShowNPC(chr, n); });

            //if (WeatherID != 0) MapPacket.SendWeatherEffect(ID, chr);
        }

        public bool MakeWeatherEffect(int itemID, string message, TimeSpan time, bool admin = false)
        {
            if (WeatherID != 0) return false;
            WeatherID = itemID;
            WeatherMessage = message;
            WeatherAdmin = admin;
            //MapPacket.SendWeatherEffect(ID);
            //TimerFunctions.AddTimedFunction(delegate { StopWeatherEffect(); }, time, new TimeSpan(), BetterTimerTypes.Weather, ID, 0);
            return true;
        }

        public void StopWeatherEffect()
        {
            WeatherID = 0;
            WeatherMessage = "";
            WeatherAdmin = false;
            //MapPacket.SendWeatherEffect(ID);
        }

        public void UpdateMobControl(Mob mob, bool spawn, Character chr)
        {
            int maxpos = 200000;
            foreach (Character c in Characters)
            {
                //int curpos = mob.mPosition - c.Position;
                //if (curpos < maxpos)
                //{
                //    mob.setControl(c, spawn, chr);
                //    return;
                //}
            }
            mob.setControl(null, spawn, chr);
        }
    }

    public class Foothold
    {
        public ushort ID { get; set; }
        public short X1 { get; set; }
        public short Y1 { get; set; }
        public short X2 { get; set; }
        public short Y2 { get; set; }
    }

    public class Life
    {
        public int ID { get; set; }
        public uint SpawnID { get; set; }
        public string Type { get; set; }
        public int RespawnTime { get; set; }
        public ushort Foothold { get; set; }
        public bool FacesLeft { get; set; }
        public short X { get; set; }
        public short Y { get; set; }
        public short Cy { get; set; }
        public short Rx0 { get; set; }
        public short Rx1 { get; set; }
        public byte Hide { get; set; }
    }

    public class Portal
    {
        public byte ID { get; set; }
        public short X { get; set; }
        public short Y { get; set; }
        public string Name { get; set; }
        public int ToMapID { get; set; }
        public string ToName { get; set; }
    }

    public class Seat
    {
        public byte ID { get; set; }
        public short X { get; set; }
        public short Y { get; set; }
    }
}
