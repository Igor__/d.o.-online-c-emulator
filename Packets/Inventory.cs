﻿using MySql.Data.MySqlClient;
using gameServer.Game.Map.Objects;
using gameServer.Game.Objects;
using gameServer.Servers;
using gameServer.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gameServer.Game
{
    public enum InventoryType
    {
        UNDEFINED,
        EQUIP,
        USE,
        SETUP,
        ETC,
        CASH ,
        EVAN,
        MECHANIC,
        HAKU,
        ANDROID,
        BITS,
        TOTEMS,
        EQUIPPED = -1
    }

    public class Inventory
    {
        public int Id { get; set; }

        public Dictionary<byte, Item> Items;
        public Dictionary<short, Equip> Equips;
        public Dictionary<int, short> ItemAmounts;

        public byte SlotLimit { get; private set; }
        public InventoryType InventoryType { get; private set; }

        public Inventory(InventoryType invtype, byte slotLimit = 96)
        {
            Items = new Dictionary<byte, Item>(slotLimit);
            Equips = new Dictionary<short, Equip>();

            if ((int)invtype >= 2 || (int)invtype <= 5)
                ItemAmounts = new Dictionary<int, short>();

            SlotLimit = slotLimit;
            InventoryType = invtype;
        }

        public void Load(int charId)
        {
            MasterServer.Instance.SqlConnection.RunQuery("SELECT * FROM inventories WHERE charid = " + charId + " AND type = '" + InventoryType.ToString() + "'");
            MySqlDataReader reader = MasterServer.Instance.SqlConnection.Reader;
            reader.Read();

            if (reader.HasRows)
            {
                this.Id = reader.GetInt32(0);
                this.SlotLimit = reader.GetByte("slotlimit");
                reader.Close();

                MasterServer.Instance.SqlConnection.RunQuery("SELECT * FROM inventoryequips WHERE inventoryid = " + Id);
                reader = MasterServer.Instance.SqlConnection.Reader;

                while (reader.Read())
                {
                    Equip equip = new Equip();
                    equip.Id = reader.GetInt32(0);
                    equip.InventoryId = reader.GetInt32("inventoryid");
                    equip.ItemID = reader.GetInt32("itemid");
                    equip.InventorySlot = reader.GetInt16("slot");
                    equip.Scrolls = reader.GetByte("upgradeslots");
                    equip.Level = reader.GetByte("level");
                    equip.Str = reader.GetInt16("str");
                    equip.Dex = reader.GetInt16("dex");
                    equip.Int = reader.GetInt16("int");
                    equip.Luk = reader.GetInt16("luk");
                    equip.HP = reader.GetInt16("hp");
                    equip.MP = reader.GetInt16("mp");
                    equip.Watk = reader.GetInt16("watk");
                    equip.Matk = reader.GetInt16("matk");
                    equip.Wdef = reader.GetInt16("wdef");
                    equip.Mdef = reader.GetInt16("mdef");
                    equip.Acc = reader.GetInt16("acc");
                    equip.Avo = reader.GetInt16("avoid");
                    equip.Hands = reader.GetInt16("hands");
                    equip.Speed = reader.GetInt16("speed");
                    equip.Jump = reader.GetInt16("jump");
                    equip.Locked = reader.GetByte("locked") > 0;
                    equip.ViciousHammer = reader.GetByte("vicioushammer");
                    equip.ItemLevel = reader.GetByte("itemlevel");
                    equip.ItemEXP = reader.GetInt32("itemexp");
                    equip.Expiration = reader.GetInt64("expiration");

                    Equips.Add(equip.InventorySlot, equip);
                }

                reader.Close();
            }
        }

        public void Save(int charId, bool newChr)
        {
            if (!newChr)
            MySQLTool.Delete("inventories", "charid", charId);

            StringBuilder sb = new StringBuilder();
            sb.Append("(charid, type, slotlimit) ");
            sb.Append("VALUES (");
            sb.Append(charId + ", ");
            sb.Append("'" + InventoryType.ToString() + "', ");
            sb.Append(SlotLimit + ")");
            MySQLTool.Create("inventories", sb);

            if (!newChr)
                MySQLTool.Delete("inventoryequips", "inventoryid", Id);
            //If not new char, we just save. Nothing to delete!
            if (Equips != null)
            {
                foreach (KeyValuePair<short, Equip> equip in Equips)
                {
                    //TODO: SAVE.
                    sb.Clear();
                    sb.Append("(inventoryid, itemid, slot, upgradeslots, level, str, dex, `int`, luk, hp, mp, watk, matk, wdef, mdef, acc, avoid, hands, speed, jump, locked, vicioushammer, itemlevel, itemexp, expiration) ");
                    sb.Append("VALUES (");
                    sb.Append(equip.Value.InventoryId + ", ");
                    sb.Append(equip.Value.ItemID + ", ");
                    sb.Append(equip.Value.InventorySlot + ", ");
                    sb.Append(equip.Value.Slots + ", ");
                    sb.Append(equip.Value.Level + ", ");
                    sb.Append(equip.Value.Str + ", ");
                    sb.Append(equip.Value.Dex + ", ");
                    sb.Append(equip.Value.Int + ", ");
                    sb.Append(equip.Value.Luk + ", ");
                    sb.Append(equip.Value.HP + ", ");
                    sb.Append(equip.Value.MP + ", ");
                    sb.Append(equip.Value.Watk + ", ");
                    sb.Append(equip.Value.Matk + ", ");
                    sb.Append(equip.Value.Wdef + ", ");
                    sb.Append(equip.Value.Mdef + ", ");
                    sb.Append(equip.Value.Acc + ", ");
                    sb.Append(equip.Value.Avo + ", ");
                    sb.Append(equip.Value.Hands + ", ");
                    sb.Append(equip.Value.Speed + ", ");
                    sb.Append(equip.Value.Jump + ", ");
                    sb.Append(equip.Value.Locked + ", ");
                    sb.Append(equip.Value.ViciousHammer + ", ");
                    sb.Append(equip.Value.ItemLevel + ", ");
                    sb.Append(equip.Value.ItemEXP + ", ");
                    sb.Append(equip.Value.Expiration + ")");


                    MySQLTool.Create("inventoryequips", sb);
                }
            }

            if (!newChr)
                MySQLTool.Delete("inventoryitems", "inventoryid", Id);

            if (Items != null)
            {
                foreach (KeyValuePair<byte, Item> item in Items)
                {
                    sb.Clear();
                    sb.Append("(inventoryid, itemid, slot, quantity, accountid, packageid, owner, log, uniqueid, flag, expiration, sender) ");
                    sb.Append("VALUES (");
                    sb.Append(Id + ", ");
                    sb.Append(item.Value.ItemID + ", ");
                    sb.Append(item.Value.InventorySlot + ", ");
                    sb.Append(item.Value.Quantity + ", ");
                    //Gay way of doing this, creating objects for nothing! Need to find easier way or simply add a public character list to the server!
                    //sb.Append(MySQLTool.GetCharacter(charId).AccountId + ", ");
                    sb.Append(item.Value.PackageId + ",");
                    sb.Append("'" + item.Value.Owner + "', ");
                    sb.Append("'" + item.Value.Log + "', ");
                    sb.Append(item.Value.UniqueId + ", ");
                    sb.Append(item.Value.Flag + ", ");
                    sb.Append(item.Value.Expiration + ", ");
                    sb.Append("'" + item.Value.Sender + "')");

                    MySQLTool.Create("inventoryitems", sb);
                }
            }
        }
    }
}
