﻿using MySql.Data.MySqlClient;
using gameServer.Core.IO;
using gameServer.Game;
using gameServer.Servers;
using gameServer.Tools;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gameServer.Packets.Handlers
{
	class OnPlayerQuit
	{
		public static void _buffie(MartialClient c, InPacket p)
		{
            p.Skip(4);
            byte quitClass = p.ReadByte();
            Console.WriteLine("QuitClass byte: " + quitClass);
            c.Account.Characters[quitClass].isSpawned = false;
		}
	}
}
