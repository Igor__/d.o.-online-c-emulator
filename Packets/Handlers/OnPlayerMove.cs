﻿using MySql.Data.MySqlClient;
using gameServer.Core.IO;
using gameServer.Game;
using gameServer.Servers;
using gameServer.Tools;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gameServer.Packets.Handlers
{
    class OnPlayerMove
    {
        public static void _buffie(MartialClient c, InPacket p)
        {
            Logger.WriteLog(Logger.LogTypes.HEmi, "OnPlayerMove validate");
            p.Skip(7+8);
            c.WriteRawPacket(MoveCharacterPacket.HandleMovement(c.Account, c.SessionID, p.ReadBytes(4), p.ReadBytes(4)));
        }
    }
}
