﻿using gameServer.Core.IO;
using gameServer.Tools;
using gameServer.Game;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gameServer.Packets.Handlers
{
	static class Ping
	{
		public static void _buffie(MartialClient c, InPacket p)
		{
			Logger.WriteLog(Logger.LogTypes.HEmi, "Ping validate");
			c.WriteRawPacket(new byte[0]);
		}
	}
}
