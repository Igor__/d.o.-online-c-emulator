﻿using MySql.Data.MySqlClient;
using gameServer.Core.IO;
using gameServer.Core.Cryptography;
using gameServer.Game;
using gameServer.Servers;
using gameServer.Tools;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gameServer.Packets.Handlers
{
	static class CreateNewCharacter
	{
		public static void _buffie(MartialClient c, InPacket p)
		{
			if (c.Account.LoggedIn != true) { return; }

			Logger.WriteLog(Logger.LogTypes.HEmi, "CreateNewCharacter handle");

			p.Skip(7);
			
			Character newChr = new Character(true);

			newChr.Client = c;
			byte[] charBName = new byte[16];
			charBName = p.ReadBytes(16);
			for (int i = 0; i < 16; i++)
			{
				if (charBName[i] != 0x00) continue;
				Array.Resize(ref charBName, i);
				break;
			}

			newChr.charName = Encoding.ASCII.GetString(charBName);

			if (MasterServer.Instance.SqlConnection.NameTaken(newChr.charName))
			{
				Logger.WriteLog(Logger.LogTypes.HEmi, "Player name has been already taken: {0}", newChr.charName);
                c.WriteRawPacket(Constants.createNCharNameTaken);
				return;
			}

			p.Skip(2);
			newChr.face = p.ReadByte();

			p.Skip(5);
			newChr.charClass = p.ReadByte();

			p.Skip(1);
			newChr.Str = p.ReadByte();

			p.Skip(1);
			newChr.Dex = p.ReadByte();

			p.Skip(1);
			newChr.Vit = p.ReadByte();

			p.Skip(1);
			newChr.Agi = p.ReadByte();

			p.Skip(1);
			newChr.Int = p.ReadByte();

			p.Skip(1);
			newChr.statPoints = p.ReadByte();

			newChr.faction = 0;
			newChr.level = 1;
			newChr.maxHP = 50;
			newChr.curHP = 50;
			newChr.maxMP = 50;
			newChr.curMP = 50;
			newChr.maxSP = 50;
			newChr.curSP = 50;
			newChr.atk = 50;
			newChr.def = 50;
			newChr.coin = 0;
			newChr.fame = 0;
			newChr.locX = -1660;
			newChr.locY = 2344;
			newChr.map = 1;
			newChr.skillPoints = 0;
			newChr.ownerID = c.Account.Id;

			c.Account.Characters.Add(newChr);

			if (newChr.Create() == 1)
			{
				newChr.charID = MasterServer.Instance.SqlConnection.GetLastInsertId();
				//MySQLTool.CreateInventories(newChr);
				//MySQLTool.CreateEquips(newChr, equips);
				//MySQLTool.LoadInventoriesId(newChr);
				c.WriteRawPacket(Constants.createNewCharacter);
				return;
			}

			//Logger.LogCheat(Logger.HackTypes.Login, c, "Attempted to create a character that already exists.");
			//c.Close();
			return;
		}
	}
}
