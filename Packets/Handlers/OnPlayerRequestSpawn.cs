﻿using MySql.Data.MySqlClient;
using gameServer.Core.IO;
using gameServer.Core.Cryptography;
using gameServer.Game;
using gameServer.Servers;
using gameServer.Tools;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gameServer.Packets.Handlers
{
    class OnPlayerRequestSpawn
    {
        public static void _buffie(MartialClient c, InPacket p)
        {
            for (int i = 0; i < c.Account.Characters.Count(); i++)
            {
                if (c.Account.Characters[i].isSpawned == true)
                {
                    return;
                }
            }

            p.Skip(4);

            Character target = c.Account.Characters[p.ReadByte()];
            if (target == default(Character))
            {
                Logger.WriteLog(Logger.LogTypes.HEmi, "Wrong target has been selected by packet");
                return;
            }

            target.isSpawned = true;

            byte[] first = new byte[1452];
            byte[] second = new byte[1452];
            byte[] third = new byte[1452];
            byte[] fourth = new byte[1452];

            byte[] stuffLOL = new byte[] {
				(byte)0xc0, (byte)0x16, (byte)0x00, (byte)0x00, (byte)0x04, (byte)0x00, (byte)0x01, (byte)0x00, (byte)0x01, (byte)0x00, (byte)0x00, (byte)0x00,  
				(byte)0x00, (byte)0x00, (byte)0x00, (byte)0x01, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x64, (byte)0x00, (byte)0x00, (byte)0x00, 
				(byte)0x04, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x02, (byte)0x05, (byte)0x0a};

            byte[] cha = new byte[] {
				(byte)0x0c, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x9d, (byte)0x0f, (byte)0xbf, 
				(byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x50, (byte)0x2e
			};

            byte[] coordinates = new byte[8];
            byte[] xCoord = BitTools.floatToByteArray(target.locX);
            byte[] yCoord = BitTools.floatToByteArray(target.locY);
            byte[] chID = BitTools.intToByteArray(target.charID);

            for (int i = 0; i < 4; i++)
            {
                stuffLOL[i + 12] = chID[i];
                coordinates[i] = xCoord[i];
                coordinates[i + 4] = yCoord[i];
            }

            stuffLOL[20] = (byte)0x01;

            for (int i = 0; i < stuffLOL.Length; i++)
            {
                first[i] = stuffLOL[i];
            }

            for (int i = 0; i < coordinates.Length; i++)
            {
                fourth[i + (1452 - coordinates.Length)] = coordinates[i];
            }

            c.WriteRawPacket(first);
            c.WriteRawPacket(second);
            c.WriteRawPacket(third);
            c.WriteRawPacket(fourth);
            c.WriteRawPacket(cha);
        }
    }
}