﻿using gameServer.Tools;
using System;
using System.IO;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gameServer
{
	class Constants
	{
        public static readonly string ConfigName = "Config.ini";
        public static readonly string username = m_ini.GetString(ConfigName, "Database", "Username");
        public static readonly string database = m_ini.GetString(ConfigName, "Database", "Database");
        public static readonly string password = m_ini.GetString(ConfigName, "Database", "Password");
        public static readonly string host     = m_ini.GetString(ConfigName, "Database", "Host");
        public static readonly string LogWriting = "Logs\\logs.txt";
		public static readonly string ExternalIP = IP.ExternalIPAddress();
        public static readonly string sHellPort = "666";
        public static readonly int LoginPort = 667;
        public static readonly int LLPort = 10000;
		public static readonly int CDPPort = 10002;
        public static readonly int HellPort = Convert.ToInt32(sHellPort);

        public static void initialiseConfig()
        {
            if (!m_ini.Exist(ConfigName)) m_ini.Create(ConfigName, "[Database]\r\nUsername = \"root\"\r\nPassword = \"vertrigo\"\r\nDatabase = \"brightmh\"\r\nHost = \"127.0.0.1\"");
            return;
        }

		//tell client the authentication was a huge success
		public static readonly byte[] authSuccess = new byte[] {
			(byte)0x46, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0xd9, (byte)0x00, (byte)0x1c, (byte)0x00, (byte)0x1c, (byte)0x00, 
			(byte)0x00, (byte)0x00, (byte)0x01, (byte)0x45, (byte)0xbb, (byte)0x1f, (byte)0x36, (byte)0xcb, (byte)0xfc, (byte)0x63, (byte)0x3f, (byte)0x11, (byte)0x50, (byte)0xaa, (byte)0xea, (byte)0x3a, 
			(byte)0x94, (byte)0x60, (byte)0x8e, (byte)0xed, (byte)0x0f, (byte)0x86, (byte)0xd5, (byte)0xb9, (byte)0xf1, (byte)0xd5, (byte)0x62, (byte)0xcf, (byte)0x90, (byte)0x7f, (byte)0x0e, (byte)0x00, 
			(byte)0x00, (byte)0x00, (byte)0x0d, (byte)0x87, (byte)0xc0, (byte)0x59, (byte)0x6c, (byte)0xe7, (byte)0xe8, (byte)0xd7, (byte)0xa8, (byte)0xbb, (byte)0xd8, (byte)0xce, (byte)0x15, (byte)0x6d, 
			(byte)0xac, (byte)0x83, (byte)0x21, (byte)0x4e, (byte)0x84, (byte)0x84, (byte)0x0a, (byte)0x00
		};

		//tell client the authentication was a huge fail
		public static readonly byte[] authFail = new byte[] { (byte)0x09, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x03, (byte)0x00, (byte)0x64, (byte)0x00, (byte)0xc9 };

        public static readonly byte[] accFail = new byte[] { (byte)0x09, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x03, (byte)0x00, (byte)0x64, (byte)0x00, (byte)0xCA, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00 };

		//empty account - no characters
		public static readonly byte[] account = new byte[] {
			(byte)0x44, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x03, (byte)0x00, (byte)0x05, (byte)0x00, (byte)0x62, (byte)0x61, (byte)0x75, 
			(byte)0x6b, (byte)0x32, (byte)0x33, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00,
			(byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x6d,
			(byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, 
			(byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, 
			(byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, 
			(byte)0x00, (byte)0x00
		};

		//tell client the create character was a huge success
		public static readonly byte[] createNewCharacter = new byte[] {
			(byte)0x14, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x03, (byte)0x00, (byte)0x06, (byte)0x00, (byte)0x01, (byte)0x01, 
			(byte)0x12, (byte)0x2b, (byte)0x00, (byte)0xc0, (byte)0xb7, (byte)0xc4, (byte)0x00, (byte)0xe0, (byte)0x21, (byte)0x45 
		};
        //14 00 00 00 03 00 06 00 00 CE 9F 2A 00 00 00 00 00 00 00 00 - nickname already taken
        public static readonly byte[] createNCharNameTaken = new byte[20] {
            0x14, 0x0, 0x0, 0x0, 0x03, 0x0, 0x06, 0x0, 0x0, 0xCE, 0x9F, 0x2A, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0
        };

		public static readonly byte[] quit = new byte[] { (byte)0x09, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x03, (byte)0x00, (byte)0x64, (byte)0x00, (byte)0x00 };
	}
}
