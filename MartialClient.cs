﻿using gameServer.Core.IO;
using gameServer.Core.Network;
using gameServer.Game;
using gameServer.Packets;
using gameServer.Servers;
using gameServer.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace gameServer
{
	public sealed class MartialClient : MartialSession
	{
		private PacketProcessor m_processor;
		private Func<MartialClient, bool> m_deathAction;

		public Character Character { get; set; }
		public Account Account { get; set; }

		public long SessionID { get; set; }

		public byte World { get; set; }
		public byte Channel { get; set; }

		public MartialClient(Socket client, PacketProcessor processor, Func<MartialClient, bool> death) : base(client)
		{
			m_processor = processor;
			m_deathAction = death;
		}

		protected override void OnPacket(byte[] packet)
		{
            Console.WriteLine(BitConverter.ToString(packet));
			using (InPacket p = new InPacket(packet))
			{
				byte opcode = p.ReadByte();

				PacketHandler handler = m_processor[opcode];

				if (handler != null)
				{
					handler(this, p);
				}
				else
				{
					Logger.WriteLog(Logger.LogTypes.Warning, "[{0}] Unhandled packet from {1}: {2}.", m_processor.Label, Label, p.ToString());
				}
			}
		}

		protected override void OnDisconnected()
		{
			if (Account != null)
			{
				Account.LoggedIn = false;
				Account.Save();
			}
			else return;

			if (Account.Characters != null)
				Account.Characters.Clear();

			if (Character != null)
			{
				Character.Save();

				/*if (Character.Inventory != null)
				{
					if (Character.Inventory.Count > 0)
					{
						foreach (KeyValuePair<InventoryType, Inventory> inv in Character.Inventory)
						{
							inv.Value.Save(Character.Id, false);
						}
					}
				}*/
			}

			Logger.WriteLog(Logger.LogTypes.Disconnect, "[{0}] Client {1} disconnected.", m_processor.Label, Label);
			m_deathAction(this);
		}
	}
}
