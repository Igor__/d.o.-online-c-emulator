﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace gameServer.Tools
{
	class Logger
	{
		public static Dictionary<HackTypes, MartialClient> Cheaters = new Dictionary<HackTypes, MartialClient>();

		public enum LogTypes
		{
			Debug,
			Info,
			LList,
			CDisp,
			HEmi,
			Warning,
			Error,
			Connect,
			Disconnect
		}

		public enum HackTypes
		{
			Speed,
			Dupe,
			Warp,
			Login
		}

		public static bool mCommandEnabled = false;
		public static string mCommandBuffer = "";

		public static void WriteHeader()
        {
            Console.Clear();
			ConsoleColor tmpColor = Console.ForegroundColor;
			Console.ForegroundColor = ConsoleColor.Gray;
			Console.WriteLine("\n\n\tC# Martial Heroes Emulator");
			Console.ForegroundColor = ConsoleColor.White;
			Console.WriteLine("					 ");
			Console.ForegroundColor = tmpColor;
		}

		public static void LogCheat(HackTypes type, MartialClient c, string msg = "")
		{
			Cheaters.Add(type, c);
			//TODO:
			//Log msg and such to file.
		}

		public static void WriteLog(LogTypes type, string msg, params object[] pObjects)
		{
            FileWriter.Write(Constants.LogWriting, string.Format(msg + "\n", pObjects));
			string tab = "";
			for (int i = 1; i > (type.ToString().Length / 8); i--)
			{
				tab += "\t";
			}
			if (type == LogTypes.Debug)
			{
				Console.ForegroundColor = ConsoleColor.Green;
				Console.Write("\tDebug" + tab + "| ");
			}
			else if (type == LogTypes.Info)
			{
				Console.ForegroundColor = ConsoleColor.Gray;
				Console.Write("\tInfo" + tab + "| ");
			}
			else if (type == LogTypes.LList)
			{
				Console.ForegroundColor = ConsoleColor.DarkMagenta;
				Console.Write("\tLobby" + tab + "| ");
			}
			else if (type == LogTypes.CDisp)
			{
				Console.ForegroundColor = ConsoleColor.DarkGray;
				Console.Write("\tCDisp" + tab + "| ");
			}
			else if (type == LogTypes.HEmi)
			{
				Console.ForegroundColor = ConsoleColor.Magenta;
				Console.Write("\tHell" + tab + "| ");
			}
			else if (type == LogTypes.Warning)
			{
				Console.ForegroundColor = ConsoleColor.DarkYellow;
				Console.Write("\tWarning" + tab + "| ");
			}
			else if (type == LogTypes.Error)
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.Write("\tError" + tab + "| ");
			}
			else if (type == LogTypes.Connect)
			{
				Console.ForegroundColor = ConsoleColor.DarkGreen;
				Console.Write("\tConnect" + tab + "| ");
			}
			else if (type == LogTypes.Disconnect)
			{
				Console.ForegroundColor = ConsoleColor.DarkRed;
				Console.Write("\tDisconnect" + tab + "| ");
			}

			Console.ForegroundColor = ConsoleColor.White;
			Console.WriteLine(msg, pObjects);
			if (mCommandEnabled)
				Console.Write("> {0}", mCommandBuffer);
		}

		public static void Read()
		{
			Console.Read();
		}
	}
}
