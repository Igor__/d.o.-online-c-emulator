﻿using MySql.Data.MySqlClient;
using gameServer.Game;
using gameServer.Servers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gameServer.Tools
{
	class MySQLTool
	{
		public static int Save(string db, StringBuilder data, string inc, int incVal)
		{
			MasterServer.Instance.SqlConnection.RunQuery("UPDATE " + db + " SET " + data.ToString() + " WHERE " + inc + " = " + incVal);
			return 1;
		}

		public static int Create(string db, StringBuilder data)
		{
			MasterServer.Instance.SqlConnection.RunQuery("INSERT INTO " + db + " " + data.ToString());
			return 1;
		}

		public static int Delete(string db, string value, int valueEq)
		{
			MasterServer.Instance.SqlConnection.RunQuery("DELETE FROM " + db + " WHERE " + value + " = " + valueEq);
			return 1;
		}

		public static Character GetCharacter(int charId)
		{
			Character chr = new Character(false);
			chr.Id = charId;

			return chr.Load() == 0 ? null : chr;
		}

		public static Account GetAccount(int accId)
		{
			Account acc = new Account();
			acc.Id = accId;

			return acc.Load() == 0 ? null : acc;
		}
	}
}