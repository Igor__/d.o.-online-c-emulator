﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace gameServer.Tools
{
	class BitTools
	{
		public static int byteToInt(byte b) 
		{
			return (int) b & 0xFF;
		}
		
		public static float byteArrayToFloat(byte[] b) 
		{
			return System.BitConverter.ToSingle(b, 0);
		}
		
		public static int byteArrayToInt(byte[] b) 
		{
			return BitConverter.ToInt32(b, 0);
		}
		
		public static byte[] intToByteArray(int var) 
		{
			return BitConverter.GetBytes(var);		   
		}
	
		public static byte[] shortToByteArray(short sval) 
		{
			return BitConverter.GetBytes(sval);
		}
	
		public static byte[] floatToByteArray(float f) 
		{
			return BitConverter.GetBytes(f);
		}
		
		public static byte[] stringToByteArray(String s)
		{
			return Encoding.ASCII.GetBytes(s);
		}
	  
		public static String byteArrayToString(byte[] baww) 
		{
			return Encoding.ASCII.GetString(baww);
		}
	}
}
