﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace gameServer.Tools
{
	public class MySQL_Connection
	{
		private MySqlConnection Connection { get; set; }
		private MySqlCommand Command { get; set; }
		public MySqlDataReader Reader { get; set; }
		private string mConnectString { get; set; }
		public bool mShuttingDown { get; set; }

		public MySQL_Connection(string username, string password, string database, string server, ushort port = 3306)
		{
			mShuttingDown = false;
			mConnectString = "Server=" + server + "; Port=" + port + "; Database=" + database + "; Uid=" + username + "; Pwd=" + password;
			Connect();
		}

		public void Connect()
		{
			try
			{
				//FileWriter.WriteLine("Logs\\DB_crashes.txt", string.Format("[{0}][DB LIB] Connecting...", DateTime.Now.ToString()), true);
				Connection = new MySqlConnection(mConnectString);
				//FileWriter.WriteLine("Logs\\DB_crashes.txt", string.Format("[{0}][DB LIB] State Change...", DateTime.Now.ToString()), true);
				Connection.StateChange += new System.Data.StateChangeEventHandler(Connection_StateChange);
				//FileWriter.WriteLine("Logs\\DB_crashes.txt", string.Format("[{0}][DB LIB] Opening Connection", DateTime.Now.ToString()), true);
				Connection.Open();
				//FileWriter.WriteLine("Logs\\DB_crashes.txt", string.Format("[{0}][DB LIB] Connected with MySQL server with version info: {1} and uses {2}compression", DateTime.Now.ToString(), Connection.ServerVersion, Connection.UseCompression ? "" : "no "), true);
				Console.WriteLine();
			}
			catch (MySqlException ex)
			{
				//FileWriter.WriteLine("Logs\\DB_crashes.txt", string.Format("[{0}][DB LIB] Got exception @ MySQL_Connection::Connect() : {1}", DateTime.Now.ToString(), ex.ToString()), true);
				Console.WriteLine(ex.ToString());
				//throw new Exception(string.Format("[{0}][DB LIB] Got exception @ MySQL_Connection::Connect() : {1}", DateTime.Now.ToString(), ex.ToString()));
				Connect();
			}
			catch (Exception ex)
			{
				//FileWriter.WriteLine("Logs\\DB_crashes.txt", string.Format("[{0}][DB LIB] Got exception @ MySQL_Connection::Connect() : {1}", DateTime.Now.ToString(), ex.ToString()), true);
				throw new Exception(string.Format("[{0}][DB LIB] Got exception @ MySQL_Connection::Connect() : {1}", DateTime.Now.ToString(), ex.ToString()));
			}
		}

		void Connection_StateChange(object sender, System.Data.StateChangeEventArgs e)
		{
			if (e.CurrentState == System.Data.ConnectionState.Closed && !mShuttingDown)
			{
				Logger.WriteLog(Logger.LogTypes.Info, "MySQL Connection lost, reconnecting...");
				Connection.StateChange -= Connection_StateChange;
				Connect();
			}
			else if (e.CurrentState == System.Data.ConnectionState.Open)
			{
				//empty lines pzl
				Console.WriteLine();
			}
		}

		public int RunQuery(string query)
		{
			try
			{
				if (Reader != null && !Reader.IsClosed)
				{
					Reader.Close();
					Reader.Dispose();
					Reader = null;
				}
				Command = new MySqlCommand(query, Connection);
				if (query.StartsWith("SELECT"))
				{
					Reader = Command.ExecuteReader();
					return Reader.HasRows ? 1 : 0;
				}
				else if (query.StartsWith("DELETE") || query.StartsWith("UPDATE") || query.StartsWith("INSERT"))
					return Command.ExecuteNonQuery();
			}
			catch (InvalidOperationException)
			{
				Console.WriteLine("Lost connection to DB... Trying to reconnect and wait a second before retrying to run query.");
				Connect();
				System.Threading.Thread.Sleep(1000);
				RunQuery(query);
			}
			catch (MySqlException ex)
			{
				if (ex.Number == 2055)
				{
					Console.WriteLine("Lost connection to DB... Trying to reconnect and wait a second before retrying to run query.");
					Connect();
					System.Threading.Thread.Sleep(1000);
					RunQuery(query);
				}
				else
				{
					Console.WriteLine(ex.ToString());
					Console.WriteLine(query);
					//FileWriter.WriteLine("Logs\\DB_crashes.txt", string.Format("[{0}][DB LIB] Got exception @ MySQL_Connection::RunQuery({1}) : {2}", DateTime.Now.ToString(), query, ex.ToString()), true);
					throw new Exception(string.Format("[{0}][DB LIB] Got exception @ MySQL_Connection::RunQuery({1}) : {2}", DateTime.Now.ToString(), query, ex.ToString()));
				}
			}
			return 0;
		}

		public int GetLastInsertId()
		{
			return (int)Command.LastInsertedId;
		}

		public bool Ping()
		{
			if (Reader != null && !Reader.IsClosed)
				return false;
			return Connection.Ping();
		}

		public bool NameTaken(string name)
		{
			this.RunQuery("SELECT * FROM chars WHERE charName = '" + MySqlHelper.EscapeString(name) + "'");
			MySqlDataReader reader = this.Reader;
			reader.Read();

			if (!reader.HasRows)
			{
				reader.Close();
				return false;
			}

			reader.Close();
			return true;
		}
	}
}
