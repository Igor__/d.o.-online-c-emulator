﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gameServer.Core.Cryptography
{
	public sealed class MartialCipher : IDisposable
	{
		private static readonly byte[] opcdz = new byte[]
		{
			0x08, 0x2E, 0x0C, 0x38, 0x3F, 0x3C, 0x3B
		};

		public enum TransformDirection : byte
		{
			Encrypt,
			Decrypt
		}

		private TransformDirection m_direction;

		private Action<byte[]> m_transformer;

		public TransformDirection TransformationDirection
		{
			get { return m_direction; }
		}

		public MartialCipher(TransformDirection transformDirection)
		{
			m_direction = transformDirection;

			m_transformer = m_direction == TransformDirection.Encrypt ? new Action<byte[]>(EncryptTransform) : new Action<byte[]>(DecryptTransform);
		}

		public void Transform(byte[] data)
		{
			m_transformer(data);
		}

		private void EncryptTransform(byte[] data)
		{
			Cryptography.Encryptor.Encrypt(data);
		}

		private void DecryptTransform(byte[] data)
		{
			Cryptography.Decryptor.Decrypt(data);
		}

		public static int GetPacketLength(byte[] packetHeader)
		{
			return (packetHeader[0] + (packetHeader[1] << 8)) ^ (packetHeader[2] + (packetHeader[3] << 8));
		}

		public void Dispose()
		{
			m_transformer = null;
		}
	}
}
