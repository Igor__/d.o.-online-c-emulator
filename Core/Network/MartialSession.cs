﻿using gameServer.Core.Cryptography;
using gameServer.Tools;
using gameServer.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace gameServer.Core.Network
{
	public abstract class MartialSession
	{
		public string Label { get; private set; }

		private readonly Socket m_socket;

		private MartialCipher m_sendCipher;
		private MartialCipher m_recvCipher;

		private bool m_header;
		private byte[] m_hbuffer;
		private int m_offset;
		private byte[] m_buffer;

		private object m_locker;
		private bool m_disposed;

		protected abstract void OnPacket(byte[] packet);
		protected abstract void OnDisconnected();

		public bool Connected
		{
			get
			{
				return !m_disposed;
			}
		}

		public MartialSession(Socket socket)
		{
			m_socket = socket;
			m_socket.NoDelay = true;
			m_socket.ReceiveBufferSize = 0xFFFF;
			m_socket.SendBufferSize = 0xFFFF;

			m_locker = new object();
			m_disposed = false;

			Label = ((IPEndPoint)(m_socket.RemoteEndPoint)).Address.ToString();

			m_sendCipher = new MartialCipher(MartialCipher.TransformDirection.Encrypt);
			m_recvCipher = new MartialCipher(MartialCipher.TransformDirection.Decrypt);

			WaitForData(true, 8); // cause of Martial Packet Header size
		}

		private void WaitForData(bool header, int size)
		{
			if (m_disposed) { return; }

			m_header = header;
			m_offset = 0;
			m_buffer = new byte[size];

			BeginRead(m_buffer.Length);
		}

		private void BeginRead(int size)
		{
			SocketError outError = SocketError.Success;

			m_socket.BeginReceive(m_buffer, m_offset, size, SocketFlags.None, out outError, ReadCallback, null);

			if (outError != SocketError.Success)
			{
				Close();
			}
		}

		private void ReadCallback(IAsyncResult iar)
		{
			if (m_disposed) { return; }

			SocketError error;
			int received = m_socket.EndReceive(iar, out error);

			if (received == 0 || error != SocketError.Success)
			{
				Close();
				return;
			}

			m_offset += received;

			if (m_offset == m_buffer.Length)
			{
				HandleStream();
			}
			else
			{
				BeginRead(m_buffer.Length - m_offset);
			}
		}

		private void HandleStream()
		{
			if (m_header)
			{
				int size = MartialCipher.GetPacketLength(m_buffer);

				if (size > m_socket.ReceiveBufferSize)
				{
					Close();
					return;
				}

				m_hbuffer = m_buffer;
				WaitForData(false, size-8);
			}
			else
			{
				m_recvCipher.Transform(m_buffer);
				OnPacket(m_hbuffer.Concat(m_buffer).ToArray());
				WaitForData(true, 8);
			}
		}

		public void WritePacket(params byte[][] packets)
		{
			if (m_disposed) { return; }

			lock (m_locker)
			{
				int length = 0;
				int offset = 0;

				foreach (byte[] buffer in packets)
				{
					length += 8;				//header length
					length += buffer.Length;	//packet length
				}

				byte[] finalPacket = new byte[length];

				foreach (byte[] buffer in packets)
				{
					//m_sendCipher.GetHeaderToClient(finalPacket, offset, buffer.Length);

					offset += 8; //header space

					//m_sendCipher.Transform(buffer);
					Buffer.BlockCopy(buffer, 0, finalPacket, offset, buffer.Length);

					offset += buffer.Length; //packet space
				}

				WriteRawPacket(finalPacket); //send the giant crypted packet
			}
		}

		public void WriteRawPacket(byte[] packet)
		{
			if (m_disposed) { return; }

			int offset = 0;

			while (offset < packet.Length)
			{
				SocketError outError = SocketError.Success;
				int sent = m_socket.Send(packet, offset, packet.Length - offset, SocketFlags.None, out outError);

				if (sent == 0 || outError != SocketError.Success)
				{
					Close();
					return;
				}

				offset += sent;
			}
		}

		public void Close()
		{
			Dispose();
		}

		public void Dispose()
		{
			if (!m_disposed)
			{
				m_disposed = true;

				m_socket.Shutdown(SocketShutdown.Both);
				m_socket.Close();

				m_offset = 0;
				m_buffer = null;

				OnDisconnected();
			}
		}
	}
}