﻿using gameServer.Core.Network;
using gameServer.Core;
using gameServer.Tools;
using gameServer.Packets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace gameServer.Servers
{
	class HellEmissary
	{
		private Acceptor m_acceptor;
		private List<MartialClient> m_clients;
		private PacketProcessor m_processor;

		public HellEmissary(short port)
		{
			m_acceptor = new Acceptor(port);
			m_acceptor.OnClientAccepted = OnClientAccepted;

			m_clients = new List<MartialClient>();

			SpawnHandlers();
		}

		private void SpawnHandlers()
		{
			m_processor = new PacketProcessor("Login");

            m_processor.AppendHandler(RecvOps.Validate, Packets.Handlers.LoginHandler.LauncherValidate);
			m_processor.AppendHandler(RecvOps.Ping, Packets.Handlers.Ping._buffie);
			m_processor.AppendHandler(RecvOps.OnPlayaQuit, Packets.Handlers.OnPlayerQuit._buffie);
			m_processor.AppendHandler(RecvOps.CreateNewCharacter, Packets.Handlers.CreateNewCharacter._buffie);
            m_processor.AppendHandler(RecvOps.OnPlayaReqSpawn, Packets.Handlers.OnPlayerRequestSpawn._buffie);
            m_processor.AppendHandler(RecvOps.OnPlayaMove, Packets.Handlers.OnPlayerMove._buffie);
		}
		
		private void OnClientAccepted(Socket client)
		{
			MartialClient mc = new MartialClient(client, m_processor, m_clients.Remove);
			m_clients.Add(mc);

			Logger.WriteLog(Logger.LogTypes.HEmi, "Client {0} connected to Hell Emissary.", mc.Label);
			mc.WriteRawPacket(Constants.authSuccess);
			mc.Account = new Game.Account();
		}

		public void Run()
		{
			m_acceptor.Start();
			Logger.WriteLog(Logger.LogTypes.Info, "HellEmissary        - {0}.", m_acceptor.Port);
		}

		public void Shutdown()
		{
			m_acceptor.Stop();
			foreach (MartialClient mc in m_clients)
				mc.Close();

			//m_clients.Clear();
		}
	}
}