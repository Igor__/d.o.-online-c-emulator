﻿using gameServer.Core.Network;
using gameServer.Core;
using gameServer.Packets;
using gameServer.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace gameServer.Servers
{
	class LobbyListener
	{
		private Acceptor m_acceptor;
		private List<MartialClient> m_clients;
		private PacketProcessor m_processor;

		public LobbyListener(short port)
		{
			m_acceptor = new Acceptor(port);
			m_acceptor.OnClientAccepted = OnClientAccepted;

			m_clients = new List<MartialClient>();
		}

		private void OnClientAccepted(Socket client)
		{
			MartialClient mc = new MartialClient(client, m_processor, m_clients.Remove);
			byte[] pckt = new byte[16];
			pckt[0] = (byte)pckt.Length; //packet length
			pckt[4] = 0x01; //amount of servers
			pckt[8] = 0x02; //server names are hard coded in client itself. they are distinctable by this byte 
			pckt[12] = 0x01; //server status
			mc.WriteRawPacket(pckt);
			Logger.WriteLog(Logger.LogTypes.LList, "Client {0} handled - end connection.", mc.Label);
			mc.Close();
			//mc.Account = new Game.Account();
		}

		public void Run()
		{
			m_acceptor.Start();
            Logger.WriteLog(Logger.LogTypes.Info, "LListener           - {0}.", m_acceptor.Port);
		}

		public void Shutdown()
		{
			m_acceptor.Stop();
			foreach (MartialClient mc in m_clients)
				mc.Close();

			//m_clients.Clear();
		}
	}
}
