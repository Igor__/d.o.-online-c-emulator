﻿using gameServer.Core;
using gameServer.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace gameServer.Servers
{
	class MasterServer
	{
		public static MasterServer Instance { get; set; }

		public bool Running { get; private set; }

		public LobbyListener LobbyListener { get; private set; }
		public ConnectionDispatcher ConnectionDispatcher { get; private set; }
        public LoginServer LoginServer { get; private set; }
		public HellEmissary HellEmissary { get; private set; }

		public MySQL_Connection SqlConnection { get; set; }

		public MasterServer()
		{
            SqlConnection = new MySQL_Connection(Constants.username, Constants.password, Constants.database, Constants.host);

			LobbyListener = new LobbyListener((Int16)Constants.LLPort);
			ConnectionDispatcher = new ConnectionDispatcher((Int16)Constants.CDPPort);
            LoginServer = new LoginServer((Int16)Constants.LoginPort);
			HellEmissary = new HellEmissary((Int16)Constants.HellPort);
		}

		public void Run()
		{
			DateTime start = DateTime.Now;
			//Provider.Cache();
			DateTime finish = DateTime.Now;

			LobbyListener.Run();
			ConnectionDispatcher.Run();
            LoginServer.Run();
			HellEmissary.Run();

			Running = true;

			Logger.WriteLog(Logger.LogTypes.Debug, "External IP address: {0}", Constants.ExternalIP);
		}

		public void Shutdown()
		{
			Console.WriteLine("Shutting down MasterServer");

			if(LobbyListener != null) LobbyListener.Shutdown();
            if(ConnectionDispatcher != null) ConnectionDispatcher.Shutdown();
            if(HellEmissary != null) HellEmissary.Shutdown();

			Running = false;

			Console.WriteLine("gameServer is offline.");
		}
	}
}
