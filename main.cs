﻿using gameServer.Servers;
using gameServer.Core;
using gameServer.Tools;
using System;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Timers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gameServer
{
	class main
	{
		static void Main(string[] args)
		{
			Console.ForegroundColor = ConsoleColor.White;
			Timer titleUpdate = new Timer();
			titleUpdate.Interval = 15000;
			titleUpdate.Elapsed += titleUpdate_Elapsed;
			titleUpdate.Start();
			UpdateTitle();

			MasterServer.Instance = new MasterServer();

			Logger.WriteHeader();
			Logger.WriteLog(Logger.LogTypes.Info, "BrightMH.gameServer - v" + "0" + "." + "1" + " started.");
			MasterServer.Instance.Run();
			Logger.mCommandEnabled = true;
			Console.Write("> ");

			while (true)
			{
				ConsoleKeyInfo key = Console.ReadKey(true);
				char chr = key.KeyChar;
				if (key.Key == ConsoleKey.Backspace)
				{
					if (Logger.mCommandBuffer.Length > 0)
					{
						Logger.mCommandBuffer = Logger.mCommandBuffer.Remove(Logger.mCommandBuffer.Length - 1);
					}
					Console.CursorLeft = 0;
					var ret = String.Format("> {0}  ", Logger.mCommandBuffer);
					Console.Write(ret);
					Console.CursorLeft = ret.Length - 2;
				}
				else if (key.Key != ConsoleKey.Enter)
				{
					Logger.mCommandBuffer += key.KeyChar;
					Console.Write(key.KeyChar);
				}
				else
				{
					Console.WriteLine();
					string[] cmd = Regex.Split(Logger.mCommandBuffer, " ");

					switch (cmd[0])
					{
						case "register":
                            if (cmd.Length == 1)
                            {
                                Logger.WriteLog(Logger.LogTypes.Info, "register name pass uniqid");
                                break;
                            }
                            //MasterServer.Instance.Database.RegisterAccount(cmd[1], cmd[2], cmd[3], true);
							Logger.WriteLog(Logger.LogTypes.Info, "Account {0} created.", cmd[1]);
							break;
                        case "message":
                            break;
						case "help":
							Logger.WriteLog(Logger.LogTypes.Info, "Available commands: Register.");
							break;
						default:
							Logger.WriteLog(Logger.LogTypes.Info, "Command does not exist. For a list, type help.");
							break;
					}

					Logger.mCommandBuffer = Logger.mCommandBuffer.Remove(Logger.mCommandBuffer.Length - 1);
				}
			}
		}

		private static void titleUpdate_Elapsed(object sender, ElapsedEventArgs e)
		{
			UpdateTitle();
		}

		static void UpdateTitle()
		{
			GC.Collect();
			Console.Title = "BrightMH - C# Martial Heroes Emulator | Memory Usage: " + Math.Round((double)GC.GetTotalMemory(false) / 1024) + "KB";
		}
	}
}
