﻿using MySql.Data.MySqlClient;
using gameServer.Servers;
using gameServer.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gameServer.Game
{
	public sealed class Character
	{
		public int Id { get; set; }
		public byte[] SP { get; set; }
		public Character(bool newChr)
		{
			SP = new byte[10];
			//Inventory = new Dictionary<InventoryType, Inventory>();
			isNew = newChr;
		}

		//public Dictionary<InventoryType, Inventory> Inventory { get; private set; }

		public MartialClient Client { get; set; }

		public int charID { get; set; }
		public string charName { get; set; }
		public byte charClass { get; set; }
		public byte face { get; set; }
		public byte faction { get; set; }
		public byte level { get; set; }
		public int maxHP { get; set; }
		public int curHP { get; set; }
		public int maxMP { get; set; }
		public int curMP { get; set; }
		public int maxSP { get; set; }
		public int curSP { get; set; }
		public int atk { get; set; }
		public int def { get; set; }
		public uint coin { get; set; }
		public uint fame { get; set; }
		public int locX { get; set; }
		public int locY { get; set; }
		public short map { get; set; }
		public short Int { get; set; }
		public short Agi { get; set; }
		public short Vit { get; set; }
		public short Dex { get; set; }
		public short Str { get; set; }
		public short statPoints { get; set; }
		public short skillPoints { get; set; }
		public int ownerID { get; set; }

		public bool isNew { get; set; }
		public bool isSpawned { get; set; }

		public int Create()
		{
			StringBuilder sb = new StringBuilder();
			sb.Append("(charName, charClass, face, faction, level, maxHP, curHP, maxMP, curMP, maxSP, curSP, atk, def, coin, fame, locX, locY, map, Inte, Agi, Vit, Str, Dex, statPointz, skillPointz, ownerID) ");
			sb.Append("VALUES (");
			sb.Append("'" + charName + "',");
			sb.Append(charClass + ",");
			sb.Append(face + ",");
			sb.Append(faction + ",");
			sb.Append(level + ",");
			sb.Append(maxHP + ",");
			sb.Append(curHP + ",");
			sb.Append(maxMP + ",");
			sb.Append(curMP + ",");
			sb.Append(maxSP + ",");
			sb.Append(curSP + ",");
			sb.Append(atk + ",");
			sb.Append(def + ",");
			sb.Append(coin + ",");
			sb.Append(fame + ",");
			sb.Append(locX + ",");
			sb.Append(locY + ",");
			sb.Append(map + ",");
			sb.Append(Int + ",");
			sb.Append(Agi + ",");
			sb.Append(Vit + ",");
			sb.Append(Str + ",");
			sb.Append(Dex + ",");
			sb.Append(statPoints + ",");
			sb.Append(skillPoints + ",");
			sb.Append(ownerID);
			sb.Append(")");
			return MySQLTool.Create("chars", sb);
		}

		public int Load()
		{
			MasterServer.Instance.SqlConnection.RunQuery("SELECT * FROM chars WHERE charID = " + Id);
			MySqlDataReader reader = MasterServer.Instance.SqlConnection.Reader;
			reader.Read();

			if (!reader.HasRows)
			{
				reader.Close();
				return 0;
			}
			else
			{
                charName = reader.GetString("charName");
                charClass = reader.GetByte("charClass");
                face = reader.GetByte("face");
                faction = reader.GetByte("faction");
                level = reader.GetByte("level");
                maxHP = reader.GetInt16("maxHP");
                curHP = reader.GetInt16("curHP");
                maxMP = reader.GetInt16("maxMP");
                curMP = reader.GetInt16("curMP");
                maxSP = reader.GetInt16("maxSP");
                curSP = reader.GetInt16("curSP");
                atk = reader.GetInt16("atk");
                def = reader.GetInt16("def");
                coin = reader.GetUInt32("coin");
                fame = reader.GetUInt32("fame");
                locX = reader.GetInt16("locX");
                locY = reader.GetInt16("locY");
                map = reader.GetInt16("map");
                Int = reader.GetInt16("Inte");
                Agi = reader.GetInt16("Agi");
                Vit = reader.GetInt16("Vit");
                Str = reader.GetInt16("Str");
                Dex = reader.GetInt16("Dex");
                statPoints = reader.GetInt16("statPointz");
                skillPoints = reader.GetInt16("skillPointz");
                ownerID = reader.GetInt32("ownerID");
                reader.Close();
			}
			return 1;
		}

		public void Save()
		{
			StringBuilder sb = new StringBuilder();
			/*sb.Append("accountid = " + AccountId);
			sb.Append(", world = " + WorldId);
			sb.Append(", name = '" + Name + "'");
			sb.Append(", level = " + level);
			sb.Append(", exp = " + EXP);
			sb.Append(", str = " + Str);
			sb.Append(", dex = " + Dex);
			sb.Append(", `int` = " + Int);
			sb.Append(", luk = " + Luk);
			sb.Append(", hp = " + HP);
			sb.Append(", maxhp = " + MaxHP);
			sb.Append(", mp = " + MP);
			sb.Append(", maxmp = " + MaxMP);
			sb.Append(", meso = " + Meso);
			sb.Append(", hpApUsed = " + ApHp);
			sb.Append(", job = " + Job);
			sb.Append(", gender = " + Gender);
			sb.Append(", fame = " + Fame);
			sb.Append(", hair = " + HairId);
			sb.Append(", face = " + FaceId);
			sb.Append(", faceMarking = " + FaceMarking);
			sb.Append(", ap = " + AP);
			sb.Append(", map = " + MapId);
			sb.Append(", spawnpoint = " + SpawnPoint);
			sb.Append(", party = 0"); // TODO: Party.
			sb.Append(", buddyslots = 20"); // TODO: Buddylist.
			sb.Append(", guildid = 0"); // TODO: Guild.
			sb.Append(", guildrank = 0");
			sb.Append(", alliancerank = 0"); // TODO: Alliance.*/
			MySQLTool.Save("chars", sb, "id", Id);
		}

		public void Delete()
		{
			MasterServer.Instance.SqlConnection.RunQuery("DELETE FROM chars WHERE id = " + Id);
		}
	}
}
