﻿using SharpEnd.Core.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpEnd.Game
{
    public class Pos // FFF
    {
        public short X { get; set; }
        public short Y { get; set; }
        public Pos()
        {
            X = 0;
            Y = 0;
        }

        public Pos(Pos basePos)
        {
            X = basePos.X;
            Y = basePos.Y;
        }

        public Pos(short x, short y)
        {
            X = x;
            Y = y;
        }

        public Pos(InPacket pr)
        {
            X = pr.ReadShort();
            Y = pr.ReadShort();
        }

        public Pos(InPacket pr, bool isInt)
        {
            X = (short)pr.ReadInt();
            Y = (short)pr.ReadInt();
        }

        public static int operator -(Pos p1, Pos p2)
        {
            return (int)(Math.Sqrt(Math.Pow((float)(p1.X - p2.X), 2) + Math.Pow((float)(p1.Y - p2.Y), 2)));
        }
    }

    public class MovableLife
    {
        public byte mStance { get; set; }
        public short mFoothold { get; set; }
        public Pos mPosition { get; set; }
        public Pos Wobble { get; set; }
        public byte Jumps { get; set; }

        public MovableLife()
        {
            mStance = 0;
            mFoothold = 0;
            mPosition = new Pos();
            Wobble = new Pos(0, 0);
        }

        public MovableLife(short FH, Pos Position, byte Stance)
        {
            mStance = Stance;
            mPosition = new Pos(Position);
            mFoothold = FH;
            Wobble = new Pos(0, 0);
        }

        public bool isFacingRight()
        {
            return mStance % 2 == 0;
        }
    }
}
