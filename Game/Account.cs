﻿using MySql.Data.MySqlClient;
using gameServer.Servers;
using gameServer.Core;
using gameServer.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gameServer.Game
{
	public sealed class Account
	{
		public int Id { get; set; }

		public string Username { get; set; }
		public string Password { get; set; }
		public string PIC { get; set; }

		public string LastIP { get; set; }

		public byte Gender { get; set; }

		public bool LoggedIn { get; set; }
		public bool GM { get; set; }

		public bool Banned { get; set; }
		public string BanReason { get; set; }

		public List<Character> Characters { get; set; }

		public int Load()
		{
			MasterServer.Instance.SqlConnection.RunQuery("SELECT * FROM users WHERE uid = " + Id);
			MySqlDataReader reader = MasterServer.Instance.SqlConnection.Reader;
			reader.Read();

			if (!reader.HasRows)
			{
				reader.Close();
				return 0;
			}
			else
			{
				//PIC = reader.GetString("pic");
				LastIP = "127.0.0.1"; //TODO: IP.
				//LoggedIn = reader.GetInt32("loggedin");
				//GM = reader.GetBoolean("gm");

				reader.Close();
				return 1;
			}
		}

		public void LoadCharacters(MartialClient c)
		{
			MasterServer.Instance.SqlConnection.RunQuery("SELECT charID FROM chars WHERE ownerID = " + Id + " LIMIT 5");
			MySqlDataReader reader = MasterServer.Instance.SqlConnection.Reader;
			List<int> ids = new List<int>();

			for (int i = 0; i < 5; i++)
			{
				//TODO: Character slots.
				if (!reader.Read())
					break;

				ids.Add(reader.GetInt32(0));
			}

			reader.Close();

			foreach (int id in ids)
			{
				Character chr = new Character(false);
				chr.Id = id;

				if (chr.Load() == 0)
					Logger.WriteLog(Logger.LogTypes.Error, "Could not load character with ID {0}.", chr.Id);

				c.Account.Characters.Add(chr);
			}

			ids.Clear();
		}

		public void Save()
		{
			//TODO
		}
	}
}
