﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpEnd.Game.Objects
{
    public class Item : ItemBase // TODO: Full inhereit!
    {
        public int Id { get; set; }
        public int ItemID { get; set; }
        public byte InventorySlot { get; set; }
        public short Quantity { get; set; }
        public int AccountId { get; set;}
        public int PackageId { get; set;}
        public string Owner { get; set;}
        public string Log { get; set;}
        public long UniqueId { get; set;}
        public int Flag { get; set;}
        public long Expiration { get; set; }
        public string Sender { get; set; }

        public Item()
        {
            ItemID = 0;
            Quantity = 0;
            AccountId = 0;
            PackageId = 0;
            Owner = "";
            Log = "";
            UniqueId = 0;
            Flag = 0;
            Expiration = 150842304000000000L;
            Sender = "";
        }

        public Item(Item itemBase)
        {
            ItemID = itemBase.ItemID;
            InventorySlot = itemBase.InventorySlot;
            Quantity = itemBase.Quantity;
            AccountId = itemBase.AccountId;
            PackageId = itemBase.PackageId;
            Owner = itemBase.Owner;
            Log = itemBase.Log;
            UniqueId = itemBase.UniqueId;
            Flag = itemBase.Flag;
            Expiration = itemBase.Expiration;
            Sender = itemBase.Sender;
        }

    }
}
